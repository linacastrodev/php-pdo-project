<?php
require_once "../../../../src/controllers/env.php";
class Connection
{
    public $connect;
    public $host = HOST;
    public $username = USER;
    public $password = PASSWORD;
    public $database = DATABASE;

    public function __construct()
    {
        $dns = "mysql:host=$this->host;dbname=$this->database;";
        try {
            $this->connect = new PDO($dns, $this->username, $this->password);
        } catch (PDOException $e) {
            exit("Error: " . $e->getMessage());
        }
    }

    public function getmyDB()
    {
        if ($this->connect instanceof PDO) {
            return $this->connect;
        }
    }
}
