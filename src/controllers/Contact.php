<?php
require_once "env.php";
require_once "interfaces/contactInterface.php";
require_once "../../../../src/model/connection.php";
/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */
class Contact implements contactInterface {
	public $db;
	function __construct() {
		$this->getConnection();
	}
	public function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}
	public function getUsers() {
		try {
			$sql = "SELECT * FROM users";
			$query = $this->db->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_OBJ);
		} catch (PDOException $e) {
			exit("Error: " . $e->getMessage());
		}
		return $results;
	}
	public function getContact($id) {
		if (isset($id)):
			try {
				$sql = "SELECT * FROM users_login where id=:id";
				$query = $this->db->prepare($sql);
				$query->execute(array(':id' => $id));
				$results = $query->fetchAll(PDO::FETCH_OBJ);
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
			return $results;
		endif;
	}
	public function insert_contacts($fk_user, $fk_contact) {
		if (isset($fk_user) || isset($fk_contact)):
			try
			{
				$sql = "INSERT INTO users_for_login (fk_user, fk_login) VALUES (:fk_user,:fk_contact)";
				$query = $this->db->prepare($sql);
				$query->execute(array(':fk_user' => $fk_user, ':fk_contact' => $fk_contact));
				if ($query) {
					header('Location: ../../../../src/views/pages/users/users.php');
				} else {
					echo "don't works";
				}
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());

			}
		endif;
	}
}

?>