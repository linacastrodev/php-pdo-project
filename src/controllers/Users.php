<?php

require_once "env.php";
require_once "interfaces/usersInterface.php";
require_once "../../../../src/model/connection.php";

/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */

class Users implements userInterfaces {
	public $db;
	public $error = null;

	function __construct() {
		$this->getConnection();

	}
	protected function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}

	public function getUsersId($id) {
		if (isset($id)):
			try {
				$sql = "SELECT users.id, users.name, users.cellphone, users.email FROM `users_for_login` inner join users on users.id = users_for_login.fk_user where users_for_login.fk_login = $id";
				$query = $this->db->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(PDO::FETCH_OBJ);
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
			return $results;
		endif;
	}

	public function insert_users_schedule($name, $email, $cellphone) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL) == true):
			if (isset($name) || isset($mail) || isset($cellphone)):
				try {
					$sql = "INSERT INTO users (name, cellphone, email) VALUES (:name,:cellphone,:email)";
					$query = $this->db->prepare($sql);
					$query->execute(array(':name' => $name, ':cellphone' => $cellphone, ':email' => $email));
					if ($query) {
						$_SESSION['error-insert'] = "Insert was success";
					} else {
						$_SESSION['error-insert'] = "Insert not was success";
					}

				} catch (PDOException $e) {
					exit("Error: " . $e->getMessage());
				}
			endif;
		endif;
	}

	public function deleteUsersId($id) {
		if (isset($id)):
			try {
				$sql = "DELETE FROM users_for_login WHERE fk_user=:id";
				$query = $this->db->prepare($sql);
				$query->execute(array(':id' => $id));
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
	}
}

?>