<?php
require_once "env.php";
require_once "interfaces/loginInterface.php";
require_once "../../../../src/model/connection.php";
/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */

class Login implements loginInterface {
	public $db;
	function __construct() {
		$this->getConnection();
	}
	public function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}
	public function validate($email, $password) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL) == true):
			if (isset($emai) || isset($password)):
				try {
					$sql = "SELECT id FROM users_login WHERE email=:email and password=:password";
					$query = $this->db->prepare($sql);
					$query->execute(array(':email' => $email, ':password' => $password));
					$results = $query->fetchAll(PDO::FETCH_OBJ);
					$_SESSION['user_session'] = $results[0]->id;
					if ($results):
						$_SESSION['error_login'] = "Login was success";
						header('Location: ../../../../src/views/pages/home/home.php');
					else:
						$_SESSION['error_login'] = "Login not was success";
					endif;
				} catch (PDOException $e) {
					exit("Error: " . $e->getMessage());
				} else :
				echo "Email don't works";
				die();
			endif;
		endif;
	}
}
?>