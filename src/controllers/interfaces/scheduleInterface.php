<?php

interface scheduleInterface {
	public function getUsersId($id);
	public function getUsers();
	public function getSchedule();
	public function insert_associate_schedule($fk_user, $fk_schedule);
	public function insert_schedule($title, $description);
}

?>