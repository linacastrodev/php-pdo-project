<?php
require_once "env.php";
require_once "interfaces/scheduleInterface.php";
require_once "../../../../src/model/connection.php";
/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */

class Schedule implements scheduleInterface {
	public $db;
	function __construct() {
		$this->getConnection();
	}
	protected function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}
	public function getUsersId($id) {
		if (isset($id)):
			try {
				$sql = "SELECT * FROM user_schedule INNER JOIN users on users.id = user_schedule.fk_user INNER JOIN schedule on schedule.id = user_schedule.fk_schedule INNER JOIN users_for_login on users_for_login.fk_user = users.id where users_for_login.fk_login = $id";
				$query = $this->db->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(PDO::FETCH_OBJ);
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
			return $results;
		endif;
	}
	public function getUsers() {
		try {
			$sql = "SELECT * FROM users";
			$query = $this->db->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_OBJ);
		} catch (PDOException $e) {
			exit("Error: " . $e->getMessage());
		}
		return $results;
	}
	public function getSchedule() {
		try {
			$sql = "SELECT * FROM schedule";
			$query = $this->db->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_OBJ);
		} catch (PDOException $e) {
			exit("Error: " . $e->getMessage());
		}
		return $results;
	}
	public function insert_associate_schedule($fk_user, $fk_schedule) {
		if (isset($fk_user) || isset($fk_schedule)):
			try
			{
				$sql = "INSERT INTO user_schedule (fk_user, fk_schedule) VALUES (:fk_user,:fk_schedule)";
				$query = $this->db->prepare($sql);
				$query->execute(array(':fk_user' => $fk_user, ':fk_schedule' => $fk_schedule));
				if ($query):
					$_SESSION['error-associate'] = "Insert was success";
				else:
					$_SESSION['error-associate'] = "Insert not was success";
				endif;
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
	}
	public function insert_schedule($title, $description) {
		if (isset($title) || isset($description)):
			try
			{
				$sql = "INSERT INTO schedule (title, description) VALUES (:title,:description)";
				$query = $this->db->prepare($sql);
				$query->execute(array(':title' => $title, ':description' => $description));
				if ($query):
					$_SESSION['error-insert-schedule'] = "Insert was success";
				else:
					$_SESSION['error-insert-schedule'] = "Insert not was success";
				endif;
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
	}
	public function deleteScheduleId($id) {
		if (isset($id)):
			try {
				$sql = "DELETE FROM user_schedule WHERE fk_user=:id";
				$query = $this->db->prepare($sql);
				$query->execute(array(':id' => $id));
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
	}
}
?>