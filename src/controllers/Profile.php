<?php

require_once "env.php";
require_once "interfaces/profileInterface.php";
require_once "../../../../src/model/connection.php";
/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */

class Profile implements profileInterface {
	public $db;
	function __construct() {
		$this->getConnection();
	}
	protected function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}
	public function getProfileId($id) {
		if (isset($id)):
			try {
				$sql = "SELECT * FROM users_login WHERE users_login.id = $id";
				$query = $this->db->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(PDO::FETCH_OBJ);
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
			return $results;
		endif;
	}
	public function logOut() {
		session_destroy();
		header('Location: ../../../../src/views/pages/login/login.php');
	}
}
?>