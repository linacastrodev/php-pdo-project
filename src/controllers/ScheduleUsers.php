<?php
require_once "env.php";
require_once "interfaces/scheduleUsersInterface.php";
require_once "../../../../src/model/connection.php";

/* ====================================
- Author: @Lirrums
- Programming Language: PHP
- Topic: PHP + OOP + PDO + MYSQL + TAILWIND + JS
- Gitlab: https://gitlab.com/linacastrodev
 ** =======================================
 */

class ScheduleUsers implements scheduleUsersInterface {
	public $db;
	function __construct() {
		$this->getConnection();
	}

	public function getConnection() {
		$this->db = new Connection();
		$this->db = $this->db->getmyDB();
	}

	public function get_users_schedule($id) {
		if (isset($id)):
			try {
				$sql = "SELECT users.id, users.name, users.cellphone, users.email FROM `users_for_login` inner join users on users.id = users_for_login.fk_user where users_for_login.fk_login = $id";
				$query = $this->db->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(PDO::FETCH_OBJ);
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
		return $results;

	}

	public function delete_users_schedule($id) {
		if (isset($id)):
			try {
				$sql = "DELETE FROM user_schedule WHERE id=:id";
				$query = $this->db->prepare($sql);
				$query->execute(array(':id' => $id));
			} catch (PDOException $e) {
				exit("Error: " . $e->getMessage());
			}
		endif;
	}
}
