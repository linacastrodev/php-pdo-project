<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
    <title>Home</title>
</head>
<body>
    <!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-purple-800 fixed bottom-0 w-full">
  <div class="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-8">
    <div class="flex items-center justify-between flex-wrap">
      <div class="w-0 flex-1 flex items-center">
        <span class="flex">
          <!-- Heroicon name: speakerphone -->
<img class="mx-auto" src="https://img.icons8.com/color/48/000000/new-contact.png">
        </span>
        <p class="ml-3 font-medium text-white truncate">
          <span class="md:hidden">
            We announced a new product!
          </span>
          <span class="hidden md:inline">
            Big news! We're excited to announce a brand new product.
          </span>
        </p>
      </div>
      <div class="order-3 mt-2 flex-shrink-0 w-full sm:order-2 sm:mt-0 sm:w-auto">
        <a href="#" class="flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-purple-600">
          Learn more
        </a>
      </div>
    </div>
  </div>
</div>
</body>
</html>