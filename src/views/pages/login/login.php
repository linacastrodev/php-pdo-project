<?php include "../../../../src/controllers/Login.php";
session_start();
$connect = new Login();
if (isset($_REQUEST['email'])):
	if (isset($_REQUEST['password'])):
		$connect->validate($_REQUEST['email'], $_REQUEST['password']);
	endif;
endif;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
    <title>Login</title>
</head>
<body>
    <main class="bg-gray-100 mb-12 sm:w-full md:max-w-md lg:max-w-lg mx-auto p-8 md:p-12 my-5 rounded-lg shadow-2xl">
        <section class="bg-purple-800 rounded-lg py-5 mx-auto">
            <form id="register" class="text-sm m-8 xs:p-4 sm:p-4 md:p-8 lg:p-8" method="POST" action="../../../../src/views/pages/login/login.php">
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="email" name="email" type="email" placeholder="E-mail Address" required="required">
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="password" name="password" type="password" placeholder="Password" required="required">
                </div>
                <div class="flex flex-wrap py-2 items-center justify-evenly">
                    <div class="sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2 h-12 text-center align-baseline">
                        <input class="bg-yellow-400 hover:bg-yellow-300 hover:text-white text-white w-full py-2 px-4 rounded" type="submit" value="Submit" />
                    </div>
                </div>
            </form>
<div class="errors-insert">
                <?php
if (isset($_SESSION["error_login"])) {
	$error = $_SESSION["error_login"];
	echo "<span>$error</span>";
}
?>
</div>
        </section>
    </main>
<?php include '../../components/footer.php';?>
</body>
</html>