<?php
session_start();
include "../../../../src/controllers/Contact.php";
$connect = new Contact();
$array = $connect->getUsers();
$array2 = $connect->getContact($_SESSION['user_session']);
if (isset($_POST['fk_user']) && isset($_POST['fk_contact'])):
	$connect->insert_contacts($_POST['fk_user'], $_POST['fk_contact']);
endif;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
    <title>Create contacts</title>
</head>
<body>
<!-- This example requires Tailwind CSS v2.0+ -->
<?php include '../../components/nav.php';?>
    <div class="w-full">
        <section class="bg-gray-100 mb-5 sm:w-full md:max-w-md lg:max-w-lg mx-auto p-2 md:p-5 my-2 rounded-lg shadow-2xl rounded-lg py-2 mx-auto mt-10">
            <h4 class="text-center text-purple-800 title-contacts">Associate Contacts</h4>
            <form id="register" class="text-sm m-5 xs:p-2 sm:p-2 md:p-2 lg:p-2" method="POST" action="associateContacts.php">
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <select class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="fk_user" name="fk_user">
                    <?php foreach ($array as $key) {?>
                                <option value="<?=$key->id?>"><? echo $key->name?></option>
                    <?php }?>
                    </select>
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <select class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="fk_contact" name="fk_contact">
                    <?php foreach ($array2 as $key) {?>
                                <option value="<?=$key->id?>"><? echo $key->email?></option>
                    <?php }?>
                    </select>
                </div>
                <div class="flex flex-wrap py-2 items-center justify-evenly">
                    <div class="sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2 h-12 text-center align-baseline">
                        <input class="bg-yellow-400 hover:bg-yellow-300 hover:text-white text-white w-full py-2 px-4 rounded" type="submit" value="New Contact" />
                    </div>
                </div>
            </form>
        </section>
    </div>
</body>
<?php include '../../components/footer.php';?>
</html>
