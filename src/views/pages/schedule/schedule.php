
<?php
session_start();
include "../../../../src/controllers/Schedule.php";
$connect = new Schedule();
$return = $connect->getUsersId($_SESSION['user_session']);
if (isset($_REQUEST['fk_user'])):
	if ($connect->deleteScheduleId($_REQUEST['fk_user'])):
		echo "Your Data Successfully Deleted";
	endif;
endif;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
    <title>Schedule</title>
</head>
<body>
<!-- This example requires Tailwind CSS v2.0+ -->
<?php include '../../components/nav.php';?>
    <div class="w-full">
      <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
<a href="associateSchedule.php" class="inline-block text-sm px-4 py-2 pt-3 leading-none border rounded text-purple-800 border-purple-800 h-10 hover:border-transparent hover:text-white hover:bg-purple-800 lg:mt-5 ml-5 mb-3">Associate Schedule</a>

        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            <tr <?php foreach ($return as $key) {?>>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="flex items-center">
                  <div class="flex-shrink-0 h-10 w-10">
                    <img class="h-10 w-10 rounded-full" src="https://img.icons8.com/color/48/000000/spyro.png" alt="">
                  </div>
                  <div class="ml-4">
                    <div class="text-sm font-medium text-gray-900">
                      <?php echo $key->email ?>
                    </div>
                    <div class="text-sm text-gray-500">
                      <?php echo $key->name ?>
                      <br>
                      <a href="tel: <?php echo $key->cellphone ?>" class="text-indigo-600 hover:text-indigo-900">Call</a>
                    </div>
                  </div>
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900"><?php echo $key->cellphone ?></div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900"><?php echo $key->title ?></div>
              </td>
              <td class="px-6 py-4">
                <div class="text-sm text-gray-900"><?php echo $key->description ?></div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-left text-sm font-medium">
                <a href="schedule.php?fk_user=<?php echo $key->fk_user; ?>"  onclick="confirm('Are you sure want to delete this record')" class="text-indigo-600 hover:text-indigo-900 button">Delete</a>
              </td>
            </tr <?php }?> >
          </tbody>
        </table>
      </div>
    </div>
</body>
<?php include '../../components/footer.php';?>
</html>
