<?php
session_start();
include "../../../../src/controllers/Schedule.php";
$connect = new Schedule();
if (isset($_POST['title']) && isset($_POST['description'])):
	$connect->insert_schedule($_POST['title'], $_POST['description']);
endif;
?>
<link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
<title>Create Schedule</title>
<?php include '../../components/nav.php';?>
    <div class="w-full">
        <section class="bg-gray-100 mb-5 sm:w-full md:max-w-md lg:max-w-lg mx-auto p-2 md:p-5 my-2 rounded-lg shadow-2xl rounded-lg py-2 mx-auto mt-10">
            <h4 class="text-center text-purple-800 title-contacts">Create Schedule</h4>
            <form id="register" class="text-sm m-5 xs:p-2 sm:p-2 md:p-2 lg:p-2" method="POST" action="createSchedule.php">
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="title" name="title" type="text" placeholder="Please enter a title" required="required">
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <textarea class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="description" name="description" type="text" placeholder="Please enter a description" required="required"></textarea>
                </div>
                <div class="flex flex-wrap py-2 items-center justify-evenly">
                    <div class="sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2 h-12 text-center align-baseline">
                        <input class="bg-yellow-400 hover:bg-yellow-300 hover:text-white text-white w-full py-2 px-4 rounded" type="submit" value="New Schedule" />
                    </div>
                </div>
            </form>
<div class="errors-insert">
                <?php
if (isset($_SESSION["error-insert-schedule"])) {
	$error = $_SESSION["error-insert-schedule"];
	echo "<span>$error</span>";
}
?>
</div>
        </section>
    </div>
<?php include '../../components/footer.php';?>