<?php
session_start();
include "../../../../src/controllers/Schedule.php";
$connect = new Schedule();
$users = $connect->getUsers();
$schedule = $connect->getSchedule();
if (isset($_POST['fk_user']) && isset($_POST['fk_schedule'])):
	$connect->insert_associate_schedule($_POST['fk_user'], $_POST['fk_schedule']);
endif;
?>
<link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
<title>Associate Schedule</title>
<?php include '../../components/nav.php';?>
    <div class="w-full">
        <section class="bg-gray-100 mb-5 sm:w-full md:max-w-md lg:max-w-lg mx-auto p-2 md:p-5 my-2 rounded-lg shadow-2xl rounded-lg py-2 mx-auto mt-10">
            <h4 class="text-center text-purple-800 title-contacts">Associate Schedule</h4>
            <form id="register" class="text-sm m-5 xs:p-2 sm:p-2 md:p-2 lg:p-2" method="POST" action="associateSchedule.php">
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <select class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="fk_user" name="fk_user">
                    <?php foreach ($users as $key) {?>
                                <option value="<?=$key->id?>"><? echo $key->name?></option>
                    <?php }?>
                    </select>
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <select class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="fk_schedule" name="fk_schedule">
                    <?php foreach ($schedule as $key) {?>
                                <option value="<?=$key->id?>"><? echo $key->title?></option>
                    <?php }?>
                    </select>
                </div>
                <div class="flex flex-wrap py-2 items-center justify-evenly">
                    <div class="sm:w-full md:w-1/2 lg:w-1/4 xl:w-1/2 h-12 text-center align-baseline">
                        <input class="bg-yellow-400 hover:bg-yellow-300 hover:text-white text-white w-full py-2 px-4 rounded" type="submit" value="New Associate schedule" />
                    </div>
                </div>
            </form>
<div class="errors-insert">
                <?php
if (isset($_SESSION["error-associate"])) {
	$error = $_SESSION["error-associate"];
	echo "<span>$error</span>";
}
?>
</div>
        </section>
    </div>
<?php include '../../components/footer.php';?>
