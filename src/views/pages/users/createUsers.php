<?php
session_start();
include "../../../../src/controllers/Users.php";
$users = new Users();
if (isset($_POST['name']) && isset($_POST['email']) && $_POST['cellphone']):
	if ($users->insert_users_schedule($_POST['name'], $_POST['email'], $_POST['cellphone'])):
		$return = $_SESSION['error'];
	endif;
endif;
?>
<link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
<title>Create Contacts</title>
<?php include '../../components/nav.php';?>
    <div class="w-full">
        <section class="bg-gray-100 mb-5 sm:w-full md:max-w-md lg:max-w-lg mx-auto p-2 md:p-5 my-2 rounded-lg shadow-2xl rounded-lg py-2 mx-auto mt-10">
            <h4 class="text-center text-purple-800 title-contacts">Create Contacts</h4>
            <form id="register" class="text-sm m-5 xs:p-2 sm:p-2 md:p-2 lg:p-2" method="POST" action="createUsers.php">
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="name" name="name" type="text" placeholder="Please enter a name" required="required">
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="email" name="email" type="email" placeholder="Please enter a email" required="required">
                </div>
                <div class="relative border rounded mb-4 shadow appearance-none label-floating">
                    <input class="w-full py-2 px-3 text-black leading-normal rounded focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:border-transparent" id="cellphone" name="cellphone" type="number" placeholder="Please enter a cellphone" required="required">
                </div>
                <div class="flex flex-wrap py-2 items-center justify-evenly">
                    <div class="sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2 h-12 text-center align-baseline">
                        <input class="bg-yellow-400 hover:bg-yellow-300 hover:text-white text-white w-full py-2 px-4 rounded" type="submit" value="New Contact" />
                    </div>
                </div>
            </form>
            <div class="errors-insert">
                <?php
if (isset($_SESSION["error-insert"])) {
	$error = $_SESSION["error-insert"];
	echo "<span>$error</span>";
}
?>
            </div>
        </section>
    </div>
<?php include '../../components/footer.php';?>
