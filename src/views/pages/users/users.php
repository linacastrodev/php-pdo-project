
<?php session_start();
include "../../../../src/controllers/Users.php";
$users = new Users();
$return = $users->getUsersId($_SESSION['user_session']);
if (isset($_REQUEST['id'])):
	if ($users->deleteUsersId($_REQUEST['id'])):
		echo "Your Data Successfully Deleted";
	endif;
endif;
?>
<link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/new-contact.png" />
<title>Contacts</title>
<?php include '../../components/nav.php';?>
    <div class="w-full">
      <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
        <a href="../contacts/associateContacts.php" class="inline-block text-sm px-4 pt-3 leading-none border rounded text-purple-800 border-purple-800 h-10 hover:border-transparent hover:text-white hover:bg-purple-800 lg:mt-5 ml-5 mb-3">Associate Contact</a>
        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              </th>
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            <tr <?php foreach ($return as $key) {?>>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="flex items-center">
                  <div class="flex-shrink-0 h-10 w-10">
                    <img class="h-10 w-10 rounded-full" src="https://img.icons8.com/color/48/000000/spyro.png" alt="">
                  </div>
                  <div class="ml-4">
                    <div class="text-sm font-medium text-gray-900">
                      <?php echo $key->email ?>
                    </div>
                    <div class="text-sm text-gray-500">
                      <?php echo $key->name ?>
                      <br>
                      <a href="tel: <?php echo $key->cellphone ?>" class="text-indigo-600 hover:text-indigo-900">Call</a>
                    </div>
                  </div>
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900"><?php echo $key->cellphone ?></div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-left text-sm font-medium">
                <a href="users.php?id=<?php echo $key->id; ?>"  onclick="confirm('Are you sure want to delete this record')" class="text-indigo-600 hover:text-indigo-900 button">Delete</a>
              </td>
            </tr <?php }?> >
          </tbody>
        </table>
      </div>
    </div>
<?php include '../../components/footer.php';?>
